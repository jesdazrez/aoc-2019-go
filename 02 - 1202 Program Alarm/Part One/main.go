package main

import (
	"aoc-2019-go/utils"
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"
)

func scanCSV(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}

	if i := bytes.IndexByte(data, ','); i >= 0 {
		return i + 1, []byte(strings.TrimSpace(string(data[0:i]))), nil
	}

	if atEOF {
		return len(data), []byte(strings.TrimSpace(string(data))), nil
	}

	// Request more data
	return 0, nil, nil
}

type computer struct {
	memory      []int
	programSize int
	err         error
}

func (c *computer) Pointer(address int) *int {
	if address >= c.programSize {
		panic("Segmentation fault!")
	}
	return &c.memory[address]
}

func (c *computer) LoadProgram(reader io.Reader) {
	c.memory, c.err = utils.GetInputWithCustomDelimiter(reader, scanCSV)
	if c.err != nil {
		panic(c.err)
	}
	c.programSize = len(c.memory)
}

func (c *computer) ExecuteProgram() {
	if c.programSize == 0 {
		return
	}

	i := 0
	op := c.memory[i]
	for op != 99 {
		if op == 1 {
			*c.Pointer(c.memory[i+3]) = *c.Pointer(c.memory[i+1]) + *c.Pointer(c.memory[i+2])
		} else if op == 2 {
			*c.Pointer(c.memory[i+3]) = *c.Pointer(c.memory[i+1]) * *c.Pointer(c.memory[i+2])
		} else {
			panic("Unexpected opcode " + string(op) + " at: " + string(i) + "!")
		}

		i += 4
		op = c.memory[i]
	}
}

func main() {
	var c computer
	c.LoadProgram(os.Stdin)
	c.memory[1] = 12
	c.memory[2] = 2
	c.ExecuteProgram()
	fmt.Println(c.memory)
}
