package utils

import (
	"bufio"
	"io"
	"strconv"
)

// GetInputWithCustomDelimiter gets a list of delimited integers from a
// io.Reader using a bufio.SplitFunc
func GetInputWithCustomDelimiter(
	reader io.Reader,
	splitFunc bufio.SplitFunc) ([]int, error) {
	var res []int
	scanner := bufio.NewScanner(reader)
	scanner.Split(splitFunc)

	for scanner.Scan() {
		number, e := strconv.Atoi(scanner.Text())
		if e != nil {
			return res, e
		}
		res = append(res, number)
	}

	return res, scanner.Err()
}

// GetInput gets a list of line-delimited integers from a io.Reader
func GetInput(reader io.Reader) ([]int, error) {
	return GetInputWithCustomDelimiter(reader, bufio.ScanLines)
}
