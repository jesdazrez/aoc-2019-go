package main

import (
	"aoc-2019-go/utils"
	"fmt"
	"os"
)

func main() {
	input, err := utils.GetInput(os.Stdin)
	if err != nil {
		panic(err)
	}
	totalFuel := 0
	for _, number := range input {
		totalFuel += number/3 - 2
	}
	fmt.Println(totalFuel)
}
