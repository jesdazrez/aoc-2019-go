package main

import (
	"aoc-2019-go/utils"
	"fmt"
	"os"
)

func getRequiredFuel(mass int) int {
	totalRequired := 0
	mass = mass/3 - 2
	for mass >= 0 {
		totalRequired += mass
		mass = mass/3 - 2
	}
	return totalRequired
}

func main() {
	input, err := utils.GetInput(os.Stdin)
	if err != nil {
		panic(err)
	}
	totalFuel := 0
	for _, number := range input {
		totalFuel += getRequiredFuel(number)
	}
	fmt.Println(totalFuel)
}
